from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QFileDialog

from matplotlib import pyplot as plt
from window import Ui_MainWindow
from classes.BgWorker import WorkerClass
import sys
import os
import datetime
import pandas as pd
import numpy as np
from scipy import stats
import pickle
from functools import partial


def catch_exceptions(t, val, tb):
    QtWidgets.QMessageBox.critical(None, "An exception was raised", "Exception type: {}".format(t))
    old_hook(t, val, tb)


# Handles exceptions for debugging
old_hook = sys.excepthook
sys.excepthook = catch_exceptions


class Main(QtWidgets.QMainWindow):

    def __init__(self):

        QtWidgets.QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Set up signals
        self.set_up_signals()

        # Directories (current working directory, base directory of the data file, and directory of the data file)
        self._cwd = os.getcwd()
        self._baseDir = None
        self._featDir = None
        self._fileType = None

        # Get the current date and time
        self.dateTime = datetime.datetime.now().strftime("%Y%m%d_%H%M")

        # Setting up the background thread and background worker
        self.thread = QThread()
        self.worker = None

        # Main data containers
        self._data = None
        self._plotData = None

        # Variables required to keep track of the user input of what features to drop from the data
        self._selectedSeries = None
        self._selectedImuLoc = None
        self._selectedImuLocIdx = None
        self._selectedMetric = None
        self._selectedMetricIdx = None

        # This is the local container for features and trials dropped for the assessment
        self._featsToDrop = list()
        self._featsDropped = list()
        self._trialsToDrop = list()
        self._trialsDropped = list()

    def set_up_signals(self):
        """
        Method for setting up all the widget signals when the form loads
        """

        # IMPORT
        self.ui.pushButtonSelectDir.clicked.connect(self.get_feature_dir)
        self.ui.pushButtonImport.clicked.connect(self.get_import_features)

        # FEATURE EXPLORE
        self.ui.pushButtonSubjectExp.clicked.connect(self.set_subject_exp)
        self.ui.pushButtonPlot.clicked.connect(self.get_plot_data)
        self.ui.comboBoxSeries1.activated.connect(partial(self.get_update_combobox_exp, 'imu', 1))
        self.ui.comboBoxSeries2.activated.connect(partial(self.get_update_combobox_exp, 'imu', 2))
        self.ui.comboBoxImu1.activated.connect(partial(self.get_update_combobox_exp, 'metric', 1))
        self.ui.comboBoxImu2.activated.connect(partial(self.get_update_combobox_exp, 'metric', 2))
        self.ui.comboBoxMetric1.activated.connect(partial(self.get_update_combobox_exp, 'feature', 1))
        self.ui.comboBoxMetric2.activated.connect(partial(self.get_update_combobox_exp, 'feature', 2))

        # FEATURE SELECTION
        self.ui.pushButtonAddFeat.clicked.connect(self.get_add_feature_list_widget)
        self.ui.pushButtonDropFeat.clicked.connect(self.get_drop_features)
        self.ui.pushButtonAddSubject.clicked.connect(self.get_add_subject_list_widget)
        self.ui.pushButtonDropSubject.clicked.connect(self.get_drop_subjects)
        self.ui.comboBoxSeries.activated.connect(partial(self.get_update_combobox_red, 'imu'))
        self.ui.comboBoxImu.activated.connect(partial(self.get_update_combobox_red, 'metric'))
        self.ui.comboBoxMetric.activated.connect(partial(self.get_update_combobox_red, 'feature'))

        # EXPORT
        self.ui.pushButtonSelectExpDir.clicked.connect(self.get_exp_dir)
        self.ui.pushButtonExport.clicked.connect(self.get_export_files)

    # IMPORT
    @pyqtSlot()
    def get_feature_dir(self):
        """
        Button triggered method for selecting the directory whether the pickle dataframe and text file descriptor
        are contained, and extracting their directory information
        """

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.ExistingFile)
        dlg.setNameFilter("Pickle or Parquet File (*.pickle *.parquet.gzip)")

        if dlg.exec_():
            self._featDir = dlg.selectedFiles()[0]
            self._fileType = os.path.splitext(self._featDir)[1]
            self._baseDir = os.path.dirname(self._featDir)
            self.ui.lineEditImport.setText(self._featDir)
            self.ui.statusbar.showMessage('Feature Directory Selected')
        else:
            self.ui.statusbar.showMessage('Directory Closed')

    @pyqtSlot()
    def get_import_features(self):
        """
        Button triggered Method for importing the pickle feature DataFrame, most work performed on background thread by
        passing the "importFeature" method
        """

        # Creating a new worker class to run the "importFeaturesWorker" method and setup its signals
        self.worker = WorkerClass(self.import_worker, self._featDir, self._fileType)
        self.worker.finished.connect(self.import_thread_finished)
        self.worker.moveToThread(self.thread)
        self.thread.started.connect(self.worker.run_function)
        self.thread.start()

        self.ui.statusbar.showMessage('Importing Data...')

    def import_worker(self, featDir, fileType):
        """
        Worker for importing the pickle feature DataFrame, passed to BG thread by "importFeatures" method
        :param pickleDir: String [Directory to pickle containing the feature data]
        :param projectType: String [Rating or Activity types supported]
        :return: String
        """

        if fileType == '.pickle':
            self._data = pd.read_pickle(featDir)
        elif fileType == 'parquet.gzip':
            self._data = pd.read_parquet(featDir)

        # Ensuring that the multi-index is correctly named for subsequent operations
        self._data.index.set_names(['label', 'pt'], inplace=True)
        self._data.columns.set_names(['series', 'location', 'metric', 'feature'], inplace=True)

        return 'Completed Importing Data'

    def import_thread_finished(self, message):
        """
        Thread Finished method called by the background thread to indicate that the pickle file has been imported
        :param message:
        """

        def update_import_descriptor(data):

            labels = list(self._data.index.get_level_values(level='label'))
            uniqueLabels = list(set(labels))
            labelDist = {label: labels.count(label) for label in uniqueLabels}

            self.ui.listWidgetImportDesc.addItem(
                'Number of Unique Classes to Classify: %d [%s]' % (len(uniqueLabels), ', '.join(str(label) for label in uniqueLabels))
            )
            self.ui.listWidgetImportDesc.addItem(
                'Class Distribution: Total: %d, %s' % (len(labels), str(labelDist))
            )
            self.ui.listWidgetImportDesc.addItem(
                'Number of Trials: %d [%s]' % (len(self._data.index.unique(level='pt')), ', '.join(
                    str(idx) for idx in self._data.index.unique(level='pt')))
            )
            self.ui.listWidgetImportDesc.addItem(
                'Total number of Features Detected: %d' % (len(self._data.columns))
            )
            self.ui.listWidgetImportDesc.addItem(
                'Number of Unique Series Recorded: %d [%s]' % (len(self._data.columns.unique(level='series')), ', '.join(
                    list(str(idx) for idx in self._data.columns.unique(level='series'))))
            )
            self.ui.listWidgetImportDesc.addItem(
                'Number of Unique Locations Recorded: %d [%s]' % (len(self._data.columns.unique(level='location')), ', '.join(
                    list(str(idx) for idx in self._data.columns.unique(level='location'))))
            )
            self.ui.listWidgetImportDesc.addItem(
                'Number of Unique Metrics Recorded: %d [%s]' % (len(self._data.columns.unique(level='metric')), ', '.join(
                    list(str(idx) for idx in self._data.columns.unique(level='metric'))))
            )
            self.ui.listWidgetImportDesc.addItem(
                'Number of Unique Feature Types Recorded: %d [%s]' % (len(self._data.columns.unique(level='feature')), ', '.join(
                    list(str(idx) for idx in self._data.columns.unique(level='feature'))))
            )

        def update_series_combobox():
            self.ui.comboBoxSeries.clear()
            self.ui.comboBoxSeries.insertItems(1, map(str, self._data.columns.unique(level='series').tolist()))
            self.get_update_combobox_red('imu')

            self.ui.comboBoxSubject.clear()
            self.ui.comboBoxSubject.insertItems(1, map(str, sorted(self._data.index.unique(level='pt'), key=int)))

        self.ui.groupBoxImport.setDisabled(True)
        self.ui.pushButtonExport.setEnabled(True)
        self.ui.lineEditExport.setText(self._baseDir)
        update_import_descriptor(self._data)

        # Preparing for plotting
        self.ui.comboBoxSubjectExp.clear()
        self.ui.comboBoxSubjectExp.insertItems(0, ['all_trials'] + [str(pt) for pt in self._data.index.unique(level='pt').tolist()])
        self.ui.groupBoxAssessmentExp.setEnabled(True)

        # Preparing for feature reduction
        self.ui.groupBoxFeatDrop.setEnabled(True)
        self.ui.groupBoxSubjectDrop.setEnabled(True)
        update_series_combobox()

        self.ui.statusbar.showMessage(message)
        self.ui.progressBarImport.setValue(100)

    # FEATURE EXPLORER
    def set_subject_exp(self):
        """
        Button triggered method for setting the trial for plotting
        """

        def update_series_combobox(data):

            self.ui.comboBoxSeries1.clear()
            self.ui.comboBoxSeries2.clear()

            self.ui.comboBoxSeries1.insertItems(0, data.columns.unique(level='series').tolist())
            self.ui.comboBoxSeries2.insertItems(0, data.columns.unique(level='series').tolist())

            self.get_update_combobox_exp('imu', 1)
            self.get_update_combobox_exp('imu', 2)

        subject = self.ui.comboBoxSubjectExp.currentText()
        if subject == 'all_trials':
            self._plotData = self._data
        else:
            self._plotData = self._data.xs(int(subject), level='pt', drop_level=False)

        update_series_combobox(self._plotData)
        self.ui.statusbar.showMessage('Trial Selected')
        self.ui.groupBoxFeature.setEnabled(True)
        self.ui.pushButtonPlot.setEnabled(True)

    @pyqtSlot()
    def get_update_combobox_exp(self, updateType, axis):

        if axis == 1:

            # Update the combobox if that type or a type above in the chain has been changed
            if updateType == 'imu':
                self.ui.comboBoxImu1.clear()
                self.ui.comboBoxImu1.insertItems(0, self._plotData[self.ui.comboBoxSeries1.currentText()].columns.unique(level='location').tolist())

            if updateType == 'imu' or updateType == 'metric':
                self.ui.comboBoxMetric1.clear()
                self.ui.comboBoxMetric1.insertItems(1, self._plotData.loc[:, (self.ui.comboBoxSeries1.currentText(), self.ui.comboBoxImu1.currentText())].columns.unique(level='metric').tolist())

            self.ui.comboBoxFeat1.clear()
            self.ui.comboBoxFeat1.insertItems(1, self._plotData.loc[:, (self.ui.comboBoxSeries1.currentText(), self.ui.comboBoxImu1.currentText(), self.ui.comboBoxMetric1.currentText())].columns.unique(level='feature').tolist())

        elif axis == 2:

            # Update the combobox if that type or a type above in the chain has been changed
            if self.ui.comboBoxImu2.currentText() == "" or updateType == 'imu':
                self.ui.comboBoxImu2.clear()
                self.ui.comboBoxImu2.insertItems(0, self._plotData[self.ui.comboBoxSeries2.currentText()].columns.unique(level='location').tolist())

            if self.ui.comboBoxMetric2.currentText() == "" or updateType == 'imu' or updateType == 'metric':
                self.ui.comboBoxMetric2.clear()
                self.ui.comboBoxMetric2.insertItems(1, self._plotData.loc[:, (self.ui.comboBoxSeries2.currentText(), self.ui.comboBoxImu2.currentText())].columns.unique(level='metric').tolist())

            self.ui.comboBoxFeat2.clear()
            self.ui.comboBoxFeat2.insertItems(1, self._plotData.loc[:, (self.ui.comboBoxSeries2.currentText(), self.ui.comboBoxImu2.currentText(), self.ui.comboBoxMetric2.currentText())].columns.unique(level='feature').tolist())

    @pyqtSlot()
    def get_plot_data(self):

        plotFeat1 = (self.ui.comboBoxSeries1.currentText(), self.ui.comboBoxImu1.currentText(), self.ui.comboBoxMetric1.currentText(), self.ui.comboBoxFeat1.currentText())
        plotFeat2 = (self.ui.comboBoxSeries2.currentText(), self.ui.comboBoxImu2.currentText(), self.ui.comboBoxMetric2.currentText(), self.ui.comboBoxFeat2.currentText())
        ax = self.ui.widgetPlot.canvas.ax
        ax.set_xlabel('_'.join(plotFeat for plotFeat in plotFeat1))
        ax.set_ylabel('_'.join(plotFeat for plotFeat in plotFeat2))
        ax.grid(which='both')
        ax.autoscale(enable=True, tight=True)
        ax.minorticks_on()

        for label in self._plotData.index.unique(level='label').sort_values():
            try:
                ax.scatter(self._plotData.loc[label, plotFeat1], self._plotData.loc[label, plotFeat2], label=label)
            except BaseException as exception:
                self.ui.statusbar.showMessage('One of the features selected no longer exists')
                return

        labels = self._plotData.index.get_level_values(level='label').values[~np.isnan(self._plotData.loc[:, plotFeat1].values)]
        data = self._plotData.loc[:, plotFeat1].values[~np.isnan(self._plotData.loc[:, plotFeat1].values)]
        corrFeat1 = stats.spearmanr(labels, data)[0]
        labels = self._plotData.index.get_level_values(level='label').values[~np.isnan(self._plotData.loc[:, plotFeat2].values)]
        data = self._plotData.loc[:, plotFeat2].values[~np.isnan(self._plotData.loc[:, plotFeat2].values)]
        corrFeat2 = stats.spearmanr(labels, data)[0]
        corrBetweenFeat = stats.spearmanr(self._plotData.loc[:, plotFeat1], self._plotData.loc[:, plotFeat2])[0]
        textstr = '\n'.join(('Corr Feat 1 = %.2f' % corrFeat1, 'Corr Feat 2 = %.2f' % corrFeat2, 'Corr Between Feat = %.2f' % corrBetweenFeat))
        ax.text(0.87, 0.25, textstr, transform=ax.transAxes, fontsize=11, verticalalignment='top', bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5))
        ax.legend()
        self.ui.widgetPlot.canvas.draw()
        ax.clear()

    # FEATURE SELECTION
    @pyqtSlot()
    def get_update_combobox_red(self, updateType):

        if updateType == 'imu':
            self._data.sort_index(axis=1, inplace=True)
            self._data.sort_index(axis=0, inplace=True)

            self.ui.comboBoxImu.clear()
            self.ui.comboBoxImu.insertItem(1, 'All')

            self._selectedSeries = str(self.ui.comboBoxSeries.currentText())
            self.ui.comboBoxImu.insertItems(1, self._data[self._selectedSeries].columns.unique(level='location').tolist())

        if updateType == 'imu' or updateType == 'metric':
            self.ui.comboBoxMetric.clear()
            self.ui.comboBoxMetric.insertItem(1, 'All')

            self._selectedImuLoc = self.ui.comboBoxImu.currentText()
            if self._selectedImuLoc == 'All':
                self._selectedImuLocIdx = self._data.loc[:, self._selectedSeries].columns.unique(level='location')
            else:
                self._selectedImuLocIdx = self._selectedImuLoc

            self.ui.comboBoxMetric.insertItems(1, self._data.loc[:, (self._selectedSeries, self._selectedImuLocIdx)].columns.unique(level='metric').tolist())

        self.ui.comboBoxFeature.clear()
        self.ui.comboBoxFeature.insertItem(1, 'All')

        self._selectedMetric = str(self.ui.comboBoxMetric.currentText())
        if self._selectedMetric == 'All':
            self._selectedMetricIdx = self._data.loc[:, (self._selectedSeries, self._selectedImuLocIdx)].columns.unique(level='metric')
        else:
            self._selectedMetricIdx = self._selectedMetric

        self.ui.comboBoxFeature.insertItems(1, self._data.loc[:, (self._selectedSeries, self._selectedImuLocIdx, self._selectedMetricIdx)].columns.unique(level='feature').tolist())

    @pyqtSlot()
    def get_add_feature_list_widget(self):
        """
        Button triggered method for adding feature types to be dropped to the feature list widget
        Combines the IMU, Metric, and Feature into a single string and adds it to the list (if it is not already contained)
        """

        widgetItems = [self.ui.listWidgetDropFeat.item(count).text() for count in range(self.ui.listWidgetDropFeat.count())]

        selectedFeature = self.ui.comboBoxFeature.currentText()
        featureDropped = {'series': self._selectedSeries, 'imu': self._selectedImuLoc, 'metric': self._selectedMetric, 'feature': selectedFeature}
        featureString = '%s_%s_%s_%s' % (self._selectedSeries, self._selectedImuLoc, self._selectedMetric, selectedFeature)

        # Checks that the selected feature to add is not already contained in the list of features to drop or has been dropped previously
        if featureString not in widgetItems and featureDropped not in self._featsDropped:
            self.ui.statusbar.showMessage('Feature added')
            self.ui.listWidgetDropFeat.addItem(featureString)
            self._featsToDrop.append(featureDropped)
        else:
            self.ui.statusbar.showMessage('You entered a duplicate feature, please try again')

    @pyqtSlot()
    def get_drop_features(self):
        """
        Button triggered method for dropping feature types from the task dataframe
        """

        # Attempts to drop feature type based on the 7 different possible combination of conditions
        for FDD in self._featsToDrop:

            if FDD['imu'] == 'All' and FDD['metric'] != 'All' and FDD['feature'] != 'All':
                for imuLoc in self._data[FDD['series']].columns.unique(level='location').tolist():
                    self._data.drop((FDD['series'], imuLoc, FDD['metric'], FDD['feature']), axis='columns', inplace=True)

            elif FDD['imu'] != 'All' and FDD['metric'] == 'All' and FDD['feature'] != 'All':
                for metric in self._data[(FDD['series'], FDD['imu'])].columns.unique(level='metric').tolist():
                    self._data.drop((FDD['series'], FDD['imu'], metric, FDD['feature']), axis='columns', inplace=True)

            elif FDD['imu'] != 'All' and FDD['metric'] != 'All' and FDD['feature'] != 'All':
                self._data.drop((FDD['series'], FDD['imu'], FDD['metric'], FDD['feature']), axis='columns', inplace=True)

            elif FDD['imu'] != 'All' and FDD['metric'] != 'All' and FDD['feature'] == 'All':
                self._data.drop((FDD['series'], FDD['imu'], FDD['metric']), axis='columns', inplace=True)

            elif FDD['imu'] == 'All' and FDD['metric'] == 'All' and FDD['feature'] != 'All':
                self._data.drop(FDD['feature'], level='feature', axis='columns', inplace=True)

            elif FDD['imu'] == 'All' and FDD['metric'] != 'All' and FDD['feature'] == 'All':
                self._data.drop(FDD['metric'], level='metric', axis='columns', inplace=True)

            elif FDD['imu'] != 'All' and FDD['metric'] == 'All' and FDD['feature'] == 'All':
                self._data.drop((FDD['series'], FDD['imu']), axis='columns', inplace=True)

            elif FDD['imu'] == 'All' and FDD['metric'] == 'All' and FDD['feature'] == 'All':
                self._data.drop(FDD['series'], level='series', axis='columns', inplace=True)

            else:
                raise BaseException

            self.ui.listWidgetFeatRed.addItem('feature %s, %s, %s, %s dropped' % (FDD['series'], FDD['imu'], FDD['metric'], FDD['feature']))

        # Stores a list of all the features that have been dropped so that no repeats are dropped and the list is
        # stored in the text file
        self._featsDropped = self._featsDropped + self._featsToDrop
        self.ui.statusbar.showMessage('%d Features Dropped' % len(self._featsToDrop))
        self._featsToDrop = list()
        self.ui.listWidgetDropFeat.clear()

        self.ui.comboBoxSeries.clear()
        self.ui.comboBoxSeries.insertItems(1, map(str, self._data.columns.unique(level='series').tolist()))
        self.ui.comboBoxImu.clear()
        self.ui.comboBoxMetric.clear()
        self.ui.comboBoxFeature.clear()

    @pyqtSlot()
    def get_add_subject_list_widget(self):
        """
        Button triggered method for adding columns to be dropped to the column list widget
        """

        widgetItems = [self.ui.listWidgetDropSubject.item(count).text() for count in range(self.ui.listWidgetDropSubject.count())]
        selectedColumn = self.ui.comboBoxSubject.currentText()

        if selectedColumn not in widgetItems and selectedColumn not in self._trialsDropped:
            self.ui.statusbar.showMessage('Column added')
            self.ui.listWidgetDropSubject.addItem(selectedColumn)
            self._trialsToDrop.append(selectedColumn)
            if self.ui.comboBoxSubject.currentIndex() < self.ui.comboBoxSubject.count():
                self.ui.comboBoxSubject.setCurrentIndex(self.ui.comboBoxSubject.currentIndex() + 1)
        else:
            self.ui.statusbar.showMessage('You entered a duplicate column, please try again')

    @pyqtSlot()
    def get_drop_subjects(self):
        """
        Button triggered method for dropping columns from the task DataFrame
        Stores all columns that have been dropped to prevent repeats and to store in the text file
        """

        for subject in self._trialsToDrop:

            self._data.drop(int(subject), level='pt', inplace=True)
            self.ui.listWidgetFeatRed.addItem('Subject %s dropped' % subject)

        self._trialsDropped = self._trialsDropped + self._trialsToDrop
        self.ui.statusbar.showMessage('%d Trials Dropped' % len(self._trialsToDrop))
        self._trialsToDrop = list()
        self.ui.listWidgetDropSubject.clear()
        self.ui.comboBoxSubject.clear()
        self.ui.comboBoxSubject.insertItems(1, map(str, sorted(self._data.index.unique(level='pt'))))

    # EXPORT
    @pyqtSlot()
    def get_exp_dir(self):
        """
        Button triggered method for extracting the path of a custom export directory
        Stores the selected directory in the "lineEditExport" widget
        """

        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.DirectoryOnly)

        if dlg.exec_():
            baseLabelDir = dlg.selectedFiles()[0]

        self.ui.lineEditExport.setText(baseLabelDir)

    @pyqtSlot()
    def get_export_files(self):
        """
        Button trigger method for exporting the DataFrame, original, and new text file based on user input
        """

        filePath = os.path.join(self.ui.lineEditExport.text(), 'fs_' + self.dateTime)
        os.mkdir(filePath)

        exportPath = os.path.join(filePath, 'sub_features.pickle')
        txtPath = os.path.join(filePath, 'selection_description.txt')

        self._data.to_pickle(exportPath)

        # export report
        if self.ui.checkBoxNewExport.isChecked():
            textPrint = list()
            textPrint.append('SUBSET OF FEATURES SELECTED ON: ' + self.dateTime + '\n')
            textPrint.append('FEAT DIR: ' + os.path.basename(self._featDir) + '\n')
            textPrint.append('Trials Dropped: ')
            trialString = ''.join('Trial ' + trial + ', ' for trial in self._trialsDropped)
            if trialString == '':
                trialString = 'None'
            textPrint.append(trialString + '\n')
            textPrint.append('Number Feature Types Dropped: ')
            textPrint.append(str(len(self._featsDropped)) + '\n')
            textPrint.append('Features Dropped: ')
            featureString = ''.join(feature['series'] + '_' + feature['imu'] + '_' + feature['metric'] + '_' + feature['feature'] + ', ' for feature in self._featsDropped)
            if featureString == '':
                featureString = 'None'
            textPrint.append(featureString + '\n')

            with open(txtPath, 'w') as f:
                for text in textPrint:
                    f.write(text + '\n')

        self.ui.statusbar.showMessage('Files Successfully Exported')
        self.ui.listWidgetExport.addItem('Files Successfully Exported')
        self.ui.progressBarExport.setValue(100)


if __name__ == "__main__":

    # Defines a new application process
    app = QtWidgets.QApplication(sys.argv)

    # Create new UI MainWindow class object and assign the window widget to it
    mainWindow = Main()
    mainWindow.show()
    sys.exit(app.exec_())


